package com.example.ferngoody.mremergency;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class CPRActivity extends AppCompatActivity {
    Button homeBtn;
    TextView t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,cprPointDetail;
    ImageView i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i42,i122,piccpr1,piccpr2,piccpr3;
    ImageButton soundBtn;
    MediaPlayer cprSound;
    boolean checkClick= true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cpr);
        cprPointDetail=(TextView)findViewById(R.id.cprPointDetail);
        homeBtn = (Button) findViewById( R.id.homeBtn );
        soundBtn=(ImageButton)findViewById(R.id.soundBtn);
        t1=(TextView)findViewById(R.id.text01);
        t2=(TextView)findViewById(R.id.text02);
        t3=(TextView)findViewById(R.id.text03);
        t4=(TextView)findViewById(R.id.text04);
        t5=(TextView)findViewById(R.id.text05);
        t6=(TextView)findViewById(R.id.text06);
        t7=(TextView)findViewById(R.id.text07);
        t8=(TextView)findViewById(R.id.text08);
        t9=(TextView)findViewById(R.id.text09);
        t10=(TextView)findViewById(R.id.text10);
        t11=(TextView)findViewById(R.id.text11);
        t12=(TextView)findViewById(R.id.text12);
        t13=(TextView)findViewById(R.id.text13);
        t14=(TextView)findViewById(R.id.text14);
        i1=(ImageView)findViewById(R.id.pic01);
        i2=(ImageView)findViewById(R.id.pic02);
        i3=(ImageView)findViewById(R.id.pic03);
        i4=(ImageView)findViewById(R.id.pic04);
        i42=(ImageView)findViewById(R.id.pic042);
        i5=(ImageView)findViewById(R.id.pic05);
        i6=(ImageView)findViewById(R.id.pic06);
        i7=(ImageView)findViewById(R.id.pic07);
        i8=(ImageView)findViewById(R.id.pic08);
        i9=(ImageView)findViewById(R.id.pic09);
        i10=(ImageView)findViewById(R.id.pic10);
        i11=(ImageView)findViewById(R.id.pic11);
        i12=(ImageView)findViewById(R.id.pic12);
        i122=(ImageView)findViewById(R.id.pic122);
        i13=(ImageView)findViewById(R.id.pic13);
        i14=(ImageView)findViewById(R.id.pic14);
        piccpr1=(ImageView)findViewById(R.id.piccpr1);
        piccpr2=(ImageView)findViewById(R.id.piccpr2);
        piccpr3=(ImageView)findViewById(R.id.piccpr3);

        t1.setText("1.หากพบคนหมดสติ ขั้นแรกให้ตรวจสอบสภาพรอบตัว เพื่อให้มั่นใจว่าบริเวณนั้นเป็นบริเวณที่ปลอดภัย \n");
        t2.setText("2.ตรวจสอบการตอบสนองของผู้ป่วย จับที่หัวไหล่ของผู้ป่วยแล้วเขย่าตัวเพื่อเรียกให้ผู้ป่วยตอบสนอง \n");
        t3.setText("3.หากผู้ป่วยไม่มีการตอบสนองใดใด ให้ตะโกนขอความช่วยเหลือ แล้วโทรเรียกรถพยาบาล จากนั้นให้รีบเตรียมการช่วยเหลือเบื้องต้น (หากมีเครื่อง AED ติดตั้งอยู่ในบริเวณใกล้เคียง ให้รีบนำมาใช้สนับสนุนโดยเร็ว) \n");
        t4.setText("4.เตรียมผายปอด+ทำ CPR \n");
        t5.setText("5.ให้ตรวจสอบชีพจรของผู้ป่วย โดยการวางนิ้วลงที่ด้านข้างตรงกลางลำคอของผู้ป่วยซึ่งเป็นจุดที่สามารถรับรู้ถึงชีพจรได้ดีที่สุด \n");
        t6.setText("6.หากสัมผัสได้ว่าชีพจรของผู้ป่วยยังคงเต้นอยู่ แต่ผู้ป่วยไม่หายใจ ให้ทำการผายปอดทุกๆ 5 วินาที และตรวจสอบชีพจรซ้ำทุกๆ 2 นาที \n");
        i8.setImageResource(R.drawable.m20);
        t7.setText("วิธีการผายปอด (Mouth to mouth) \n" +
                "\n" +
                " 1. ใช้นิ้วโป้งและนิ้วชี้มือซ้ายบีบจมูกผู้ป่วยให้สนิท และนิ้วที่เหลือกดลงบนหน้าผาก มืออีกข้างหนึ่งเชยคางผู้ป่วยขึ้น\n" +
                " 2. ก้มหน้าลงเอาปากประกบปากผู้ป่วยให้แนบสนิท \n" +
                " 3. เป่าลมออกจากปากของเราเข้าไปในปากของผู้ป่วยช้าๆ (ใช้เวลาประมาณ 1 วินาที) จนสังเกตเห็นว่าหน้าอกของผู้ป่วยยกตัวสูงขึ้น"+"\n" +
                " 4. ถอนปากของเราออกจากปากของผู้ป่วย เพื่อปล่อยให้ลมที่เป่าเข้าไป ไหลกลับออกจากปากของผู้ป่วย" +"\n" +
                " 5. ทำการผายปอดผู้ป่วยด้วยวิธีการตามข้อ 2 ถึงข้อ 4 อีก 1 รอบ \n เมื่อเริ่มเป่าปากสักพัก ถ้าหากรู้สึกว่าลมเข้าปอดได้ไม่เต็มที่เนื่องจากมีน้ำอยู่เต็มท้อง อาจจับผู้ป่วยนอนคว่ำแล้วใช้มือ 2 ข้าง วางอยู่ใต้ท้องผู้ป่วย ยกท้องผู้ป่วยขึ้นจะช่วยไล่น้ำออกจากท้องให้ไหลออกทางปากได้ แล้วจับผู้ป่วยพลิกหงาย และทำการเป่าปากต่อไป\n"  +
                "  ถ้าคลำชีพจรไม่ได้ หรือหัวใจหยุดเต้น ให้ทำการนวดหัวใจทันที  \n");
        t8.setVisibility(View.GONE);
        t9.setVisibility(View.GONE);
        t10.setVisibility(View.GONE);
       // t8.setText("8.จากนั้นให้บีบจมูกของผู้ป่วยเพื่อไม่ให้อากาศหลุดหนีออกมาทางจมูก แล้วใช้วิธีเป่าอากาศเข้าทางปากของผู้ป่วยโดยปากของผู้ผายปอดจะต้องครอบปิดปากของผู้ป่วยทั้งหมด (หากมีหน้ากากอนามัยสำหรับผายปอด ให้นำมาสวมให้ผู้ป่วยก่อนเพื่อป้องกันโรคติดต่อ หรือสิ่งที่เกิดจากการอาเจียนของผู้ป่วย) \n");
      //  t9.setText("9.ในการผายปอดให้ผายปอด 2 ครั้งโดยมีระยะเวลา 1 วินาทีต่อครั้ง และสังเกตการณ์พองตัวของหน้าอกผู้ป่วย หากไม่พบการพองตัวขึ้นของหน้าอกผู้ป่วยให้ทำการผายปอดซ้ำถ้าจำเป็น \n");
        //t10.setText("10.หากผู้ป่วยชีพจรหยุดเต้น ให้เริ่มต้นการทำ CPR โดยมีระบบการทำคือ ปั๊มหัวใจ 30 ครั้งแล้วผายปอด 2 ครั้งจนกว่าความช่วยเหลือจะมาหรือจนกว่าจะไม่สามารถทำต่อไปได้อีกแล้ว \n");
        t11.setText("1.สำหรับการทำ CPR ขั้นแรกให้ถอดเสื้อหรืออุปกรณ์ใด ๆที่กีดขวางอยู่บนหน้าอกของผู้ป่วยออกไปเท่าที่เป็นไปได้ \n");
        t12.setText("2.จากนั้นให้วางมือโดยลากฝ่ามือจากใต้รักแร้ของผู้ป่วยมาหยุดที่กลางหน้าอก แล้ววางบริเวณฐานของผ่ามือให้อยู่เหนือกระดูกสันอกของผู้ป่วย จากนั้นวางมืออีกข้างทับมือที่วางไว้ในลักษณะกุมเข้าเพื่อคอยสนับสนุนมืออีกข้าง \n");
        t13.setText("3.จากนั้นให้ยืดแขนให้ตรงแล้วจัดตำแหน่งให้หัวไหล่อยู่เหนือหน้าอกของผู้ป่วย เมื่อได้ตำแหน่งแล้วให้ออกแรงกดลงบนหน้าอกของผู้ป่วย (สำหรับผู้ป่วยที่เป็นผู้ใหญ่ จำเป็นต้องกดให้หน้าอกจมลงไปราวๆ 5 เซนติเมตรเป็นอย่างน้อย) \n");
        t14.setText("4.ความถี่ที่จำเป็นต่อการปั๊มหัวใจนั้นอยู่ที่ 100 ครั้งต่อนาทีเป็นอย่างน้อย (ควรนับออกมาดังๆเพื่อรักษาความถี่และจังหวะ) \n");

        i1.setImageResource(R.drawable.e1);
        i2.setImageResource(R.drawable.e2);

        i4.setImageResource(R.drawable.e4e1);
        i42.setImageResource(R.drawable.e4e2);
        i5.setImageResource(R.drawable.e5);
        i6.setImageResource(R.drawable.e6);
        i7.setImageResource(R.drawable.e7);

        i10.setImageResource(R.drawable.e10);

        i12.setImageResource(R.drawable.e12);
        i122.setImageResource(R.drawable.e12e2);
        i13.setImageResource(R.drawable.e13);

        cprPointDetail.setText("1.วัดเหนือปลายกระดูกหน้าอกขึ้นมา2นิ้วมือ \n 2.ใช้สันมือข้างที่ไม่ถนัดวางบนตำแหน่ง (อย่าใช้กระดูกนิ้วมือกดซี่โครง) \n 3.วางตัวในแนวตั้งฉากกับลำตัวของผู้ป่วย กดลงไปในแนวดิ่ง และอย่ากระแทก");

        piccpr1.setImageResource(R.drawable.cprpoint1);
        piccpr2.setImageResource(R.drawable.cprpoint2);
        piccpr3.setImageResource(R.drawable.cprpoint3);
         cprSound = MediaPlayer.create(this,R.raw.sound01);
        soundBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cprSound.getCurrentPosition()==cprSound.getDuration()){
                    Log.d("sound","playing end");
                    cprSound.stop();
                    try {
                        cprSound.prepare();
                    }
                    catch(Exception e){
                        Log.d("sound","error on prepare");
                    }
                    cprSound.start();
                }

                else if(!checkClick){ // if checkClick is false
                    cprSound.pause();
                    soundBtn.setImageResource(R.drawable.soundstop);
                    checkClick=true;
                    Log.d("sound","เข้า if บน");
                }
                else if(checkClick) {
                    cprSound.start();
                    soundBtn.setImageResource(R.drawable.soundplay);
                    checkClick = false;
                    Log.d("sound","เข้า if ล่าง");
                }

            }
        });

        cprSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                soundBtn.setImageResource(R.drawable.soundstop);
            }
        });


        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        cprSound.stop();
        Intent letter = new Intent(this,MainActivity.class);
        startActivity(letter);
        finish();
    }
}
