package com.example.ferngoody.mremergency;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Diango on 1/19/2018.
 */

public class GpsTracker extends Service implements LocationListener {

    //
    //  init variable
    //
    private final Context mContext;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GpsTracker(Context context) {
        this.mContext = context;
        getLocation();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        try{
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        } catch (Exception e){
            Log.e( "onLocationChanged:", e.getMessage() );
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i("LocationListener", "onStatusChanged");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.i("LocationListener", "onProviderEnabled");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i("LocationListener", "onProviderDisabled");
    }

    @SuppressLint("MissingPermission")
    public Location getLocation() {
        try {

            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // get GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // get network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // ถ้า network ปิดอยู่
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (locationManager != null) {

                        location = locationManager.getLastKnownLocation( LocationManager.NETWORK_PROVIDER );
                            if (location != null) {
                                this.canGetLocation = true;
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            } else {
                                this.canGetLocation = false;
                            }

                    }
                }

                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                this.canGetLocation = true;
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            } else {
                                this.canGetLocation = false;
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            Log.e( "getLocation Exception", e.getMessage() );
            this.canGetLocation = false;
            e.printStackTrace();
        }
        return location;
    }

    public boolean canGetLocation() {

        boolean tempGetLocation = this.canGetLocation;
        Log.v( "Can get location is : ", String.valueOf(tempGetLocation));
        return tempGetLocation;
    }

    public double getLatitude(){
        if(location != null) {
            latitude = location.getLatitude();
        }
        Log.v( "Your latitude is : ", String.valueOf(latitude));
        return latitude;
    }

    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
        return longitude;
    }

}
