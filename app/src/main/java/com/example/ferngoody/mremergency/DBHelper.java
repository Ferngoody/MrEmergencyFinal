package com.example.ferngoody.mremergency;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.content.SharedPreferences;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 * Created by Ferngoody on 14/1/2561.
 */

public class DBHelper extends SQLiteOpenHelper{
    private final String TAG = getClass().getSimpleName();
    public ArrayList<HashMap<String, String>> myfriendlist,emergencyList,userList;
    public ArrayList<HashMap<String, String>> myuserlist;
    private SQLiteDatabase sqLiteDatabase;
    private  String tablename="nothingtablecancreateineachfile";



   // public DBHelper(Context context ) { // FOR FRIEND TABLE  ต้องมีการเรียกพวกconstructorถึงจะมีารสร้างตารางขึ้นมา  คือคอนสตรั้คเต้อหนึ่งอันคือการเรียกonCreateขึ้มาครั้งนึง ซึ่งแม่งเรียกสามตาราง

//        super(context, Friend.DATABASE_NAME, null, Friend.DATABASE_VERSION); ////////////////////// Do something
//        tablename="friend";

   // }
    public DBHelper(Context c , String a){ //for USER TABLE
        super(c, User.DATABASE_NAME, null, User.DATABASE_VERSION);
        tablename="user";

    }

    public DBHelper(Context c , String a, String h){ //for EMERGENCY TABLE
        super(c, EmergencyCall.DATABASE_NAME, null, EmergencyCall.DATABASE_VERSION);
        tablename="emergency";

    }


    @Override
    public void onCreate(SQLiteDatabase db) { //สร้างตาราง

         if(tablename.equals("user")) {

            String CREATE_USER_TABLE = String.format("CREATE TABLE %s " + "(%s INTEGER PRIMARY KEY  AUTOINCREMENT,%s TEXT, %s TEXT, %s TEXT)",
                    User.TABLE,
                    User.Column.ID,
                    User.Column.PROVINCE,
                    User.Column.DISTRICT,
                    User.Column.LATITUDE);
//                Friend.Column.TEL,
//                Friend.Column.EMAIL,
//                Friend.Column.DESCRIPTION);
            Log.i(TAG, CREATE_USER_TABLE);
            //สร้างไว้ตรงนี้ sharepreference ไว้ตรงนี้ ต้องล้างแอพถึงจะเหน

            db.execSQL(CREATE_USER_TABLE);
        }

        else if(tablename.equals("emergency")) {


            String CREATE_EMERGENCY_CALL = String.format("CREATE TABLE %s " + "(%s INTEGER PRIMARY KEY  AUTOINCREMENT,%s TEXT, %s TEXT, %s TEXT, %s TEXT)",
                    EmergencyCall.TABLE,
                    EmergencyCall.Column.ID,
                    EmergencyCall.Column.DEPARTMENT_NAME,
                    EmergencyCall.Column.CALLNUMBER,
                    EmergencyCall.Column.E_DISTRICT,
                    EmergencyCall.Column.E_PROVINCE
            );

//                Friend.Column.TEL,
//                Friend.Column.EMAIL,
//                Friend.Column.DESCRIPTION);
            Log.i(TAG, CREATE_EMERGENCY_CALL);
            //สร้างไว้ตรงนี้ sharepreference ไว้ตรงนี้ ต้องล้างแอพถึงจะเหน

            db.execSQL(CREATE_EMERGENCY_CALL);
        }



    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { //อัพเดทตาราง

        String DROP_FRIEND_TABLE = "DROP TABLE IF EXISTS friend";

        db.execSQL(DROP_FRIEND_TABLE);
        Log.i(TAG, "Upgrade Database from " + oldVersion + " to " + newVersion);

        onCreate(db);
    }

    //==================================================SETTING USER ADDRESS=====get,update=,addใช้updateนะ ใช้updateตัวเดียวพอน่าจะได้=============================================================

    public ArrayList<HashMap<String,String>> getUserCurrentAddress() { //เรียกข้อมูลที่ตั้งปัจจุบันของผู้ใช้มาดู
        List<String> users = new ArrayList<String>();
        HashMap<String,String> myuser = new HashMap<>();

        sqLiteDatabase = this.getWritableDatabase();

        myuserlist = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.query
                (User.TABLE, null, User.Column.ID+"= ?"/*หัวคอลัม*/, new String[] {"1"}/*=คำในคอลัมที่จะค้น*/, null, null, null);

        if (cursor != null) {
            Log.v("move","move cursor to first");
            cursor.moveToFirst();
        }
        int i=0;
        while(!cursor.isAfterLast()) {

//            friends.add(cursor.getLong(0) + " " +
//                    cursor.getString(1) + " " +
//                    cursor.getString(2));

            myuser = new HashMap<>();
            myuser.put("id", String.valueOf(cursor.getLong(0)));
            myuser.put("province",cursor.getString(1));
            myuser.put("district",cursor.getString(2));
            myuser.put("latlong",cursor.getString(3));
//            myfriend.put("tel",cursor.getString(4));
//            myfriend.put("type",cursor.getString(5));
            myuserlist.add(myuser);

            Log.v("round", String.valueOf(i));
            i++;

            cursor.moveToNext();
        }

        sqLiteDatabase.close();

        // return friends;
        return myuserlist;
    }



    public void deleteCurrentUserAddress(User user) {   //ลบก่อน แล้วค่อยเพิ่ม  เซตไอดี ละค่อยสั่งมาลบตรงนี้นะ   THIS METHOD IS WORKING OK
        sqLiteDatabase = this.getWritableDatabase();

        sqLiteDatabase.delete(User.TABLE, User.Column.ID + " = " + user.getId(), null);

        sqLiteDatabase.close();
        //====== already delete==============//

//
//
//        ContentValues values = new ContentValues();
//        values.put(User.Column.ID, user.getId());
//        values.put(User.Column.PROVINCE, user.getProvince());
//        values.put(User.Column.DISTRICT, user.getDistrict());
//        values.put(User.Column.LATITUDE, user.getLatLong());
////        values.put(Friend.Column.EMAIL, friend.getEmail());
////        values.put(Friend.Column.DESCRIPTION, friend.getDescription());
//
//        sqLiteDatabase.insert(User.TABLE, null, values);    // เพิ่มใหม่จ้า
//        sqLiteDatabase.close();
    }

    public void addCurrentUserAddress(User user){   // THIS METHOD IS WORKING OK
        sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(User.Column.ID, user.getId());
        values.put(User.Column.PROVINCE, user.getProvince());
        values.put(User.Column.DISTRICT, user.getDistrict());
        values.put(User.Column.LATITUDE, user.getLatLong());

        sqLiteDatabase.insert(User.TABLE, null, values);

        sqLiteDatabase.close();
    }

    public void updateCurrentUserAddress(User user){ //อัปเดทตามไอดี   THIS METHOD IS WORKING

        sqLiteDatabase  = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(User.Column.ID, user.getId());
        values.put(User.Column.PROVINCE, user.getProvince());
        values.put(User.Column.DISTRICT, user.getDistrict());
        values.put(User.Column.LATITUDE, user.getLatLong());

        sqLiteDatabase.update(User.TABLE,
                values,
                EmergencyCall.Column.ID + " = ? ",
                new String[] { String.valueOf(user.getId()) });

        sqLiteDatabase.close();
    }


    //==================================================SETTING EMERGENCY CALL=============add,update,delete================================================
    public ArrayList<HashMap<String,String>> getEmergencyCall(String province){                 //////////// DOING

        HashMap<String,String> myfriend = new HashMap<>();


        sqLiteDatabase = this.getWritableDatabase();

        emergencyList= new ArrayList<>();
        Cursor cursor = sqLiteDatabase.query
                (EmergencyCall.TABLE, null, EmergencyCall.Column.E_PROVINCE+"= ?"/*หัวคอลัม*/, new String[] {province}/*=คำในคอลัมที่จะค้น*/, null, null, null);

        if (cursor != null) {
            Log.v("move","move cursor EmergencyCall to first");
            cursor.moveToFirst();
        }
        int i=0;
        while(!cursor.isAfterLast()) {

//            friends.add(cursor.getLong(0) + " " +
//                    cursor.getString(1) + " " +
//                    cursor.getString(2));

            myfriend = new HashMap<>();
            myfriend.put("id", String.valueOf(cursor.getLong(0)));
            myfriend.put("departmentName",cursor.getString(1));
            myfriend.put("callNumber",cursor.getString(2));
            myfriend.put("e_district",cursor.getString(3));
            myfriend.put("e_province",cursor.getString(4));
            // myfriend.put("type",cursor.getString(5));
            emergencyList.add(myfriend);

            Log.v("round", String.valueOf(i));
            i++;

            cursor.moveToNext();
        }

        sqLiteDatabase.close();

        // return friends;
        return emergencyList;
    }

    public ArrayList<HashMap<String,String>> getEmergencyCall(String district, String province){                 //////////// DOING

        HashMap<String,String> myfriend = new HashMap<>();


        sqLiteDatabase = this.getWritableDatabase();

        emergencyList= new ArrayList<>();
        Cursor cursor = sqLiteDatabase.query
                (EmergencyCall.TABLE, null, EmergencyCall.Column.E_PROVINCE+"= ?"+" AND " + EmergencyCall.Column.E_DISTRICT+"= ?"/*หัวคอลัม*/, new String[] {province, district}/*=คำในคอลัมที่จะค้น*/, null, null, null);

        if (cursor != null) {
            Log.v("move","move cursor EmergencyCall to first");
            cursor.moveToFirst();
        }
        int i=0;
        while(!cursor.isAfterLast()) {

//            friends.add(cursor.getLong(0) + " " +
//                    cursor.getString(1) + " " +
//                    cursor.getString(2));

            myfriend = new HashMap<>();
            myfriend.put("id", String.valueOf(cursor.getLong(0)));
            myfriend.put("departmentName",cursor.getString(1));
            myfriend.put("callNumber",cursor.getString(2));
            myfriend.put("e_district",cursor.getString(3));
            myfriend.put("e_province",cursor.getString(4));
            // myfriend.put("type",cursor.getString(5));
            emergencyList.add(myfriend);

            Log.v("round", String.valueOf(i));
            i++;

            cursor.moveToNext();
        }

        sqLiteDatabase.close();

        // return friends;
        return emergencyList;
    }


    public void addEmergencyCall(EmergencyCall emergencycall) {  //THIS METHOD IS WORKING OK
        sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // values.put(EmergencyCall.Column.ID, emergencycall.getId());
        values.put(EmergencyCall.Column.DEPARTMENT_NAME, emergencycall.getDepartment_name());
        values.put(EmergencyCall.Column.CALLNUMBER, emergencycall.getCallnumber());
        values.put(EmergencyCall.Column.E_DISTRICT, emergencycall.getE_district());
        values.put(EmergencyCall.Column.E_PROVINCE, emergencycall.getE_province());

        sqLiteDatabase.insert(EmergencyCall.TABLE, null, values);

        sqLiteDatabase.close();
    }
    //yongyee eieih
    public void updateEmergencyCall(EmergencyCall emergencyCall){ //อัปเดทตามไอดี   THIS METHOD IS WORKING

        sqLiteDatabase  = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EmergencyCall.Column.ID, emergencyCall.getId());
        values.put(EmergencyCall.Column.DEPARTMENT_NAME, emergencyCall.getDepartment_name());
        values.put(EmergencyCall.Column.CALLNUMBER, emergencyCall.getCallnumber());
        values.put(EmergencyCall.Column.E_DISTRICT, emergencyCall.getE_district());
        values.put(EmergencyCall.Column.E_PROVINCE, emergencyCall.getE_province());

        sqLiteDatabase.update(EmergencyCall.TABLE,
                values,
                EmergencyCall.Column.ID + " = ? ",
                new String[] { String.valueOf(emergencyCall.getId()) });

        sqLiteDatabase.close();
    }

    public  void deleteEmergencyCall(EmergencyCall emergencyCall){ //deleteตามNo อยู่นะ  ก้เซตไอดีที่จะลบก่อน แล้วค่อยเอาobject ส่งมาลบตรงนี้  THIS METHOD IS WORKING OK
        sqLiteDatabase = this.getWritableDatabase();

        sqLiteDatabase.delete(EmergencyCall.TABLE, EmergencyCall.Column.ID + " = " + emergencyCall.getId(), null);

        sqLiteDatabase.close();


    }
}
