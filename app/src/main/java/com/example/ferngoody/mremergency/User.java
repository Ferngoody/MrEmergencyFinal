package com.example.ferngoody.mremergency;
import android.provider.BaseColumns;

/**
 * Created by Ferngoody on 14/1/2561.
 */

public class User {
    private int id;
    private String cur_province;
    private String cur_district;
    private String latLong;

    public static final String DATABASE_NAME = "ADDRESS_USER.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE = "user";

    public class Column {
        public static final String ID = BaseColumns._ID;
        public static final String PROVINCE = "province";
        public static final String DISTRICT = "district";
        public static final String LATITUDE = "latitude_longtitude";
//        public static final String EMAIL = "email";
//        public static final String DESCRIPTION = "description";
    }



    //Default Constructor
    public User() {


    }
    //Constructor
    public User( int id, String province, String district,  String latitudelongtitude) {

        this.id = id;
        this.cur_district=district;
        this.cur_province=province;
        this.latLong=latitudelongtitude;

    }

    //Getter, Setter


    public String getProvince() {
        return cur_province;
    }

    public String getDistrict() {
        return cur_district;
    }

    public String getLatLong() {
        return latLong;
    }
    public int getId() {
        return id;
    }



    public void setCur_province(String province) {
        this.cur_province = province;
    }

    public void setCur_District(String district) {
        this.cur_district = district;
    }

    public void setLatLong(String mlatLong) {
        this.latLong = mlatLong;
    }




    public void setId(int id) {
        this.id = id;
    }
}
