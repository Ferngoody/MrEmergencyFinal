package com.example.ferngoody.mremergency;

import android.widget.Button;

/**
 * Created by Ferngoody on 18/1/2561.
 */

public class BTNode {
    private BTNode firstNode;
    private BTNode secondNode;
    private String detail;
    private Button btn;
    public BTNode( BTNode f ,BTNode s,String detail,Button btnn){

        this.firstNode=f;
        this.secondNode=s;
        this.detail=detail;
        this.btn=btnn;
    }
    public void setFirst(BTNode f){
        firstNode=f;
    }
    public void setSecond(BTNode s){
        secondNode=s;
    }
    public void setDetail(String dd){
        detail=dd;
    }
    public void setBTN(Button bttn){
        btn=bttn;
    }
    public BTNode getFirst(){
        return firstNode;
    }
    public BTNode getSecond(){
        return secondNode;
    }
    public String getDetail(){
        return detail;
    }
    public Button getBTN(){
        return  btn;
    }
}
