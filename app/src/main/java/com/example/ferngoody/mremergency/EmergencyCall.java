package com.example.ferngoody.mremergency;
import android.provider.BaseColumns;

/**
 * Created by Ferngoody on 14/1/2561.
 */

public class EmergencyCall {
    private int id;
    private String department_name;
    private String callnumber;
    private String e_district;
    private String e_province;

    public static final String DATABASE_NAME = "EMERGENCY_CALL.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE = "EmergenyCall";

    public class Column {
        public static final String ID = BaseColumns._ID;
        public static final String DEPARTMENT_NAME = "departmentName";
        public static final String CALLNUMBER = "callnumber";
        public static final String E_DISTRICT = "e_district";
        public static final String E_PROVINCE = "e_province";
//        public static final String EMAIL = "email";
//        public static final String DESCRIPTION = "description";
    }



    //Default Constructor
    public EmergencyCall() {


    }
    //Constructor
    public EmergencyCall(int id, String departmentname, String callnumber, String e_district, String e_province) {

        this.id = id;
        this.department_name=departmentname;
        this.callnumber=callnumber;
        this.e_district=e_district;
        this.e_province=e_province;

    }

    //Getter, Setter


    public String getDepartment_name() {
        return department_name;
    }

    public String getCallnumber() {
        return callnumber;
    }

    public String getE_district() {
        return e_district;
    }

    public String getE_province(){return e_province;}
    public int getId() {
        return id;
    }



    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public void setCallnumber(String callnumber) {
        this.callnumber = callnumber;
    }

    public void setE_district(String e_district) {
        this.e_district = e_district;
    }
    public void setE_province(String e_province){this.e_province=e_province;}




    public void setId(int id) {
        this.id = id;
    }
}
